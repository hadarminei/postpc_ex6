package exercise.android.reemh.todo_items

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.util.*

class MainActivity: AppCompatActivity() {

    var todosHolder: TodoItemsHolder? = null
    private var receiverToChanges: BroadcastReceiver? = null
    private val adapter = TodoAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        //singleton of todosHolder
        if (todosHolder == null) {
            todosHolder = TodoItemsApplication.getInstance().todosHolder
        }
        var currentItems : Vector<TodoItem> = todosHolder!!.currentItems

        // find all objects from the activities xml
        val newTaskDescription : EditText = findViewById(R.id.editTextInsertTask)
        val createTask : FloatingActionButton = findViewById(R.id.buttonCreateTodoItem)
        val recyclerTodo : RecyclerView = findViewById(R.id.recyclerTodoItemsList)

        // create objects for recycler view
        adapter.setTodo(currentItems)

        recyclerTodo.adapter = adapter
        recyclerTodo.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        class MyReceiver : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
                if (intent != null) {
                    if (intent.action.equals("Todo_changed"))
                        adapter.setTodo(todosHolder!!.currentItems) }
                }
        }

        receiverToChanges = MyReceiver()
        registerReceiver(receiverToChanges, IntentFilter("Todo_changed"));

        createTask.setOnClickListener {
            val descriptionTask: String = newTaskDescription.text.toString()
            if(descriptionTask != "") {
                todosHolder!!.addNewInProgressItem(descriptionTask)
            }
            newTaskDescription.setText("")
        }

        adapter.onItemClickCallback = { todo : TodoItem ->
            if(!todo.GetIsDone()) {
                todosHolder!!.markItemDone(todo)
            }
            else {
                todosHolder!!.markItemInProgress(todo)
            }
        }

        adapter.onDeleteClickCallback = { todo : TodoItem ->
            todosHolder!!.deleteItem(todo)
        }

        adapter.onEditClickCallback = { todo: TodoItem ->
            val editIntent = Intent(this, EditActivity::class.java)
            editIntent.putExtra("idTodo", todo.idItem);
            startActivity(editIntent);
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(receiverToChanges)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    //        outState.putSerializable("todosHolder", todosHolder)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
//        todosHolder = savedInstanceState.getSerializable("todosHolder") as TodoItemsHolder?
//        todosHolder?.let { adapter.setTodo(it.currentItems) }
    }
}