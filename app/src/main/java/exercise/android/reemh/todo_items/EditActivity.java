package exercise.android.reemh.todo_items;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;
import java.util.Vector;

public class EditActivity extends AppCompatActivity {

    TodoItemsHolder todosHolder = null;
    TodoItem todoItem = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_screen);
        EditText editDescription = findViewById(R.id.editDescription);
        CheckBox changeState = findViewById(R.id.editCheckBox);
        TextView creationTime = findViewById(R.id.creationDateInfo);
        TextView lastModifiedTime = findViewById(R.id.lastModifiedInfo);

        if (todosHolder == null) {
            todosHolder = TodoItemsApplication.getInstance().getTodosHolder();
        }

        Intent intentEdit = getIntent();
        if (todoItem == null) {
            todoItem = todosHolder.getItemById(intentEdit.getStringExtra("idTodo"));
        }

        editDescription.setText(todoItem.GetDescription());
        changeState.setChecked(todoItem.GetIsDone());
        creationTime.setText(todoItem.GetCreationTime());

        DateTodoItem lastModified = new DateTodoItem(todoItem.GetUpdateTime());
        DateTodoItem timeNow = new DateTodoItem(Calendar.getInstance().getTime().toString());

        setLastModified(lastModified, timeNow);

        //set the change description listener
        editDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                todosHolder.editDescription(todoItem, editDescription.getText().toString());
                Vector<TodoItem> allitems = todosHolder.getCurrentItems();
                todoItem.SetUpdateTime(Calendar.getInstance().getTime());
                setLastModified(lastModified, timeNow);
            }
        });

        //sets checkCox change listener
        changeState.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                todosHolder.markItemDone(todoItem);
            } else {
                todosHolder.markItemInProgress(todoItem);
            }
            todoItem.SetUpdateTime(Calendar.getInstance().getTime());
            setLastModified(lastModified, timeNow);
        });

    }

    public void setLastModified(DateTodoItem prevTime, DateTodoItem now) {
        TextView lastModifiedTime = findViewById(R.id.lastModifiedInfo);
        if (prevTime.day < now.day || prevTime.month < now.month) {
            lastModifiedTime.setText(prevTime.day + "/" + prevTime.month + " at " + prevTime.hour + ":" + prevTime.minutes);
        } else if (prevTime.hour < now.hour) {
            lastModifiedTime.setText("Today at " + prevTime.hour + ":" + prevTime.minutes);
        } else if (prevTime.hour == now.hour) {
            lastModifiedTime.setText(now.minutes - prevTime.minutes + " minutes ago");

        }
    }

//    @Override
//    protected void onSaveInstanceState(@NonNull Bundle outState) {
//        super.onSaveInstanceState(outState);
////        outState.putSerializable("todosHolder", todosHolder);
//        outState.putSerializable("item", todoItem);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(@NonNull Bundle savesInstanceState) {
//        super.onRestoreInstanceState(savesInstanceState);
////        todosHolder = (TodoItemsHolder) savesInstanceState.getSerializable("todosHolder");
//        todoItem = (TodoItem) savesInstanceState.getSerializable("item");
//
//        //find the views
//        TextView creationTime = findViewById(R.id.creationDateInfo);
//        TextView lastModifiedTime = findViewById(R.id.lastModifiedInfo);
//        CheckBox changeState = findViewById(R.id.editCheckBox);
//        EditText editDescription = findViewById(R.id.editDescription);
//
//        //set the views
//        editDescription.setText(todoItem.GetDescription());
//        creationTime.setText(todoItem.GetCreationTime());
//        lastModifiedTime.setText(todoItem.GetUpdateTime());
//        changeState.setChecked(todoItem.GetIsDone());
//
//    }
}
