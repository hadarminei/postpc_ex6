package exercise.android.reemh.todo_items;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class TodoItem implements Serializable {

    private final String idItem;
    private final boolean isDone;
    private String description;
    private final String creationTime;
    private String updateTime;

    TodoItem(String id, boolean isDone, String description, String creationTime, String updateTime) {
        this.idItem = id;
        this.isDone = isDone;
        this.description = description;
        this.creationTime = creationTime;
        this.updateTime = updateTime;
    }

    /**
     * Getter function for the id of the item
     * @return The id of the item
     */
    public String getIdItem() {
        return idItem;
    }

    /**
     * Getter function for the state of the item
     * @return The state of the item
     */
    boolean GetIsDone() {
        return isDone;
    }

    /**
     * Getter function for the decsription of the item
     * @return The description of the item
     */
    String GetDescription() {
        return description;
    }

    /**
     * Setter function for the decsription of the item
     * @param new_description The description of the item
     */
    void SetDescription(String new_description) {
        description = new_description;
    }

    String GetCreationTime() {
        return creationTime;
    }

    String GetUpdateTime() {
        return updateTime;
    }

    void SetUpdateTime(Date newTime) {
        updateTime = newTime.toString();
    }

    public String generateString() {
        return idItem + "#" + isDone + "#" + description + "#" +
                creationTime + "#" + updateTime;
    }

    public static TodoItem stringToTodo(String stringTodo) {
        try {
            String[] splitTodo = stringTodo.split("#");
            String idItem = splitTodo[0];
            Boolean isDone = Boolean.parseBoolean(splitTodo[1]);
            String description = splitTodo[2];
            String creationTime = splitTodo[3];
            String updateTime = splitTodo[4];
            return new TodoItem(idItem, isDone, description, creationTime, updateTime);
        } catch (Exception e) {
            System.out.println("caught Excetpion");
            return null;
        }
    }

}
