package exercise.android.reemh.todo_items;

import android.app.Application;

public class TodoItemsApplication extends Application {

    private TodoItemsHolder todosHolder;

    public TodoItemsHolder getTodosHolder(){
        return todosHolder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        todosHolder = new TodoItemsHolderImpl(this);
    }

    private static TodoItemsApplication instance = null;

    public static TodoItemsApplication getInstance() {
        return instance;
    }

}
