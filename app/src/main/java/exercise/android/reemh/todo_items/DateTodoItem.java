package exercise.android.reemh.todo_items;

import android.util.Log;

import java.util.Date;
import java.util.HashMap;

public class DateTodoItem {
    private HashMap<String, Integer> months = new HashMap<String, Integer>() {{
        put("Jan", 1);
        put("Feb", 2);
        put("Mar", 3);
        put("Apr", 4);
        put("May", 5);
        put("Jun", 6);
        put("Jul", 7);
        put("Aug", 8);
        put("Sep", 9);
        put("Oct", 10);
        put("Nov", 11);
        put("Dec", 12);
    }};
    public int month;
    public int day;
    public int hour;
    public int minutes;

    DateTodoItem(String date) {
        String[] seperatedDate = date.split(" ");

        try {
            this.month = months.get(seperatedDate[1]);
            this.day = Integer.parseInt(seperatedDate[2]);
            String hourAndMinutes = seperatedDate[3];
            String[] parseHourMinutes = hourAndMinutes.split(":");
            this.hour = Integer.parseInt(parseHourMinutes[0]);
            this.minutes = Integer.parseInt(parseHourMinutes[1]);
            Log.e("day", String.valueOf(this.day));
        }
        catch (Exception e) {
            System.out.println("exception");
        }
    }
}
